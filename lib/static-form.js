var Backbone = require('backbone');
var $ = require('jquery');
var _ = require('underscore');
var ui = require('./ui');

var formHeadTemplate = require('./hbs/static-form-head');

var UIStaticForm = {

    render:function(json, callback){

        json = _.extend({
            attributes:{},
            validation:{},
            formErrors:{},
            isValid:false,
            validate:false,
            formTagId:"form"
        }, json);

        if(json.validate == true){
            json.formErrors = ui.Model.validate(json.attributes, json.validation);
            if(json.formErrors._isValid == false){
                json.isValid = false;
            }else{
                json.isValid = true;
            }
        }else{

        }

        var response = {
            isValid:json.isValid,
            html:''
        };

        if(json.isValid == false){
            response.html = this.getFormHead(json);
            response.html += ui.Layout.renderStatic(json, {
                type:"form",
            });
            if(json.actions){
                response.html += ui.Layout.renderStatic({
                    children:json.actions
                });
            }
        }

        if(callback){
            callback(null, response);
        }
        return response;


    }, 

    getFormHead:function(form){
        return formHeadTemplate({
            id:form.id,
            formTagId:form.formTagId,
            attributes:JSON.stringify(form.attributes || {}),
            options:JSON.stringify(form.options || {}),
            validation:JSON.stringify(form.validation || {})
        });
    },

    templateHelper:function(data){
        var form = data.hash.form;
        this.getFormHead(data);
    },

};

ui.registerHelper('ui-form-header', _.bind(UIStaticForm.templateHelper, UIStaticForm)); 

module.exports = UIStaticForm;
