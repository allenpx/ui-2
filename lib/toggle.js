var Base = require('./base');
var $ = require('jquery');
var _ = require('underscore');

var ui = require('./ui');

var UIToggle = Base.extend({

    UIType:"toggle",

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-toggle',
            className:'Toggle',
            defaultValue:"",
            label:"Toggle",
            style:"checkbox",
            onValue:'on',
            offValue:'off'
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');

        this.toggleId = 'input_'+this.id;
        this.$toggle = $('<input/>');
        this.$toggle.attr('id', this.toggleId);


        if(this.options.description){
            this.$description = $('<div class="ui-description"></div>');
        }

        if(typeof this.options.name != 'undefined'){
            this.$toggle.attr('name', this.options.name);
        }

        if(this.options.style == "checkbox"){
            this.$toggle.attr('type', 'checkbox');
        } else if(this.options.style == "radio"){
            this.$toggle.attr('type', 'radio');
        }

        this.$label = $('<label/>');
        this.$label.attr('for', this.toggleId);

        this.$toggleIcon = $('<i class="ui-toggle-icon"></i>');

        if(this.options.inputacc){
            if(this.options.static){
                this.options.inputacc.static = true;
            }
            this.$inputAcc = new ui.Input(_.extend({

            }, this.options.inputacc));
        }


        // this.on('render', function(){
        //     this.applyModelValueToDisplay();
        //     this.validateModel();
        // });
    
    },



    render:function(){

        this._super();

        this.$el.attr("data-ui-style", this.options.style);
        if(typeof this.options.form == "string" && this.options.form != "" ){
            this.$el.attr("data-form", this.options.form);
        }
        if(this.options.style == "checkbox"){
            this.$el.addClass('ui-style-checkbox');
        } else if(this.options.style == "radio"){
            this.$el.addClass('ui-style-radio');
        }

        var classes = [];

        //append description
        if(this.options.description){
            this.$el.append(this.$description);
            this.$description.html(this.options.description);
        }

        //append input
        this.$el.append(this.$toggle);
        if(typeof this.options.value != 'undefined' && this.options.style == "radio"){
            this.$toggle.attr('value', this.options.value);
        }

        

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label); 
            this.$toggle.attr('data-label', this.options.label);
        }
        //append inputIcon
        this.$label.append(this.$toggleIcon);


        if(this.options.iconprefix){
            classes.push('ui-has-acc-left');
            var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
            this.attachAccessory(iconLeft);
            this.$toggle.before(iconLeft);
        }

        if(this.options.iconsuffix){
            classes.push('ui-has-acc-right');
            var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
            this.attachAccessory(iconRight);
            this.$toggle.after(iconRight);
        }

        if(this.options.prefix && _.isString(this.options.prefix)){
            classes.push('ui-has-acc-left');
            var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
            this.attachAccessory(prefix);
            this.$toggle.before(prefix);
        }
        if(this.options.suffix && _.isString(this.options.suffix)){
            classes.push('ui-has-acc-right');
            var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
            this.attachAccessory(suffix);
            this.$toggle.after(suffix);
        }


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            if(this.options.suffix.UIElement && this.options.suffix.UIType == "select"){
                var suffix = $('<div class="ui-acc ui-acc-select ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.render().$el);
            this.$toggle.after(suffix);
        }

        if(this.$inputAcc){
            this.$el.addClass('ui-acc-input');
            this.$label.html('&nbsp;');
            this.$el.append(this.$inputAcc.render().$el);
        }

        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }


        if(this.options.static){
            if(this.options.value){
                this.setDisplayValue(this.options.value);
            }
        }

        return this;

    },


    displayError:function(isError, message){

        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
            this.$el.removeClass('ui-error'); 
            this.$el.removeClass('ui-error-show');
        }

        if(this.isClean == true && this.options.static == false){
            return false;
        } 
        if(!this.options.static){
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$el.append(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }
        if(isError){
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },


    listenToDisplay:function(){

        this._super();

        ui.$body.on('change.'+this.id, '#'+this.toggleId, _.bind(function(event){
            this.applyDisplayValueToModel();
        }, this));


        // if(this.$inputAcc){
        //     this.listenTo(this.$inputAcc, 'focusin', function(){
        //         this.$toggle.prop('checked', true);
        //         this.applyDisplayValueToModel();
        //     })
        // }

    },

    setDisplayValue:function(value){
        if(this.$toggle){
            if(this.options.style == "radio"){
                if(value == this.options.onValue){
                    this.$toggle[0].checked = true;
                }else{
                    this.$toggle[0].checked = false;
                }
            }else{
                if(value == this.options.onValue){
                    this.$toggle[0].checked = true;
                }else{
                    this.$toggle[0].checked = false;
                }
            }
        }

    },

    getDisplayValue:function(){

        var value = this.$toggle.prop('checked');

        if(this.options.style == "radio"){
            if(this.$toggle.prop('checked') == true){
                this.trigger('activate');
            }else{
                this.trigger('deactivate');
            }
            if(value){
                return this.options.onValue;
            }else{ 
                return false;
            }
        }else{
            if(value){
                return this.options.onValue;
            }else{ 
                return this.options.offValue;
            }
        }

    },

    _active:false,
    active:function(bool){
        if(bool == true){
            this._active = true;
            this.$el.addClass('ui-active');
            this.$toggle.checked = true;
            if(!this.options.static){
                this.trigger('active');
            }
        }else if(bool == false){
            this._active = false;
            this.$toggle.checked = false;
            this.$el.removeClass('ui-active');
            if(!this.options.static){
                this.trigger('inactive');
            }
        }
        return this._active;
    },  

    onMouseDown:function(event){
        this.trigger('mousedown', event);
    },

}, {

        templateHelper:function(data){
            data.hash.static = true;
            return ui.htmlSafe(new UIToggle(data.hash).render().$el);
        },

        newFromHTML:function($element){
            var options = Base.optionsFromHTML($element);

            if($element.find('label').length){
                options.label = $element.find('label').text();
            }

            options.name = $element.find('input').attr('name');
            if($element.attr('data-ui-required')){
                options.required = true;
            }
            if($element.attr('data-ui-style') != null && $element.attr('data-ui-style') != ''){ 
                options.style = $element.attr('data-ui-style');
            }

            if(options.style != "radio"){
                options.value = $element.find('input')[0].checked;
            }

            var elementFromHTML = new UIToggle(options).render();
        }

});
 
ui.registerHelper('ui-toggle', _.bind(UIToggle.templateHelper, UIToggle)); 

module.exports = UIToggle;






