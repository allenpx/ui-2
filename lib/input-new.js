/** @jsx ui.jsx.dom */
var ui = require('./ui');
var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');
var template = require('./hbs/input');

var UIInput = Base.extend({

    UIType:"input",

	initialize:function(options){

		this._super(_.extend({
            _options:options,
            baseClass:'ui-input',
            className:'Input',
			defaultValue:"",
            type:"text",
		}, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.inputId = 'input_'+this.id;
        this.$input = $('<input/>');
        this.$input.attr('id', this.inputId);

        if(this.options.type == "password"){
            this.$input.attr('type', 'password');
        }else if(this.options.type != 'date'){
            this.$input.attr('type', this.options.type);
        }

        if(this.options.type == "date"){
            this.options.format = this.options.format || 'MM/DD/YYYY';
        }

        if(this.options.type == "date" && typeof this.options.placeholder == 'undefined'){
            this.options.placeholder = this.options.format;
        }

        
        this.$input.attr('name', this.name);

        this.$label = $('<label/>');
        this.$label.attr('for', this.inputId);

        if(this.options.readonly){
            this.$input.attr('readonly', true);
        }
        if(this.options.placeholder){
            this.$input.attr('placeholder', this.options.placeholder);
        }
        if(this.options.autocomplete){
            this.$input.attr('autocomplete', true);
        }

        if(this.options.error){
            this.options.errorValue = this.options.value;
        }

        
        // if(this.options.autoselect){
        //     this.$input.attr('onClick', 'this.setSelectionRange(0, this.value.length)');
        // }


	},

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }

    }, 



	render:function(){

        var html = this.wrap(template, this.options);
        console.log('html', html);
        this.$el.replaceWith(html);

        if(!this.options.static){
            this.listenToDisplay();
        }

		return this;
	},
 

	setDisplayValue:function(value){
		if(this.$input){
			this.$input.val(value);
			this.$input.attr('value',value);
		}
	},

	getDisplayValue:function(){
		return this.$input.val();
	},

	listenToDisplay:function(){

        this._super();

		$('body').on('change keyup', '#'+this.inputId, _.bind(function(event){
            this.dirty();
			this.applyDisplayValueToModel();
		}, this));

        if(this.options.type == "date" && typeof this.calendar == 'undefined'){
            this.calendar = new ui.Calendar({
                classes:'ui-popup',
                floatParent:this
            });
            ui.$floatingLayer.append(this.calendar.render().$el);
            this.options.externalElements = [this.calendar];
            // $('body').on('mousedown mouseup click focusin', '#'+this.calendar.id, _.bind(function(event){
            //     event.stopPropagation();
            //     console.log('test')
            //     _.defer(_.bind(function(){
            //         this.focus();
            //     },this) );
            // },this));
        }



	},

	displayError:function(isError, message){
        
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
            this.flashError();
    		if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
    			this.$errorMessage.html(message);
    			this.$el.append(this.$errorMessage);
    			return false;
    		}else{
    			this.options.error = null;
    		}
        }

		if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
		}else{
			this.$el.removeClass('ui-error');
			this.$errorMessage.remove();
		}
	},

	focus:function(event){
		this.$input.focus();
	},

    focusin:function(){
        //console.log('focusin')
        this._super();
        
    },

    focusout:function(event){
        this._super();

    }, 

	attachAccessory:function($element){
		if(process.browser){
			var id = _.uniqueId('acc');
			$element.attr('id', id);

			$('body').on('click', '#'+id, $.proxy(this.focus, this));
		}
	},

}, {

	templateHelper:function(data){
		data.hash.static = true;
        return ui.htmlSafe(new UIInput(data.hash).render().$el);
	},

});

ui.registerHelper('ui-input', _.bind(UIInput.templateHelper, UIInput)); 

module.exports = UIInput;






