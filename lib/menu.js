var $ = require('jquery');
var _ = require('underscore');
var lunr = require('lunr');


var Base = require('./base');
var ui = require('./ui');

var UIMenu = Base.extend({

    UIType:"select",

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-menu',
            className:"Menu",
            defaultValue:"",
            items:[]
        }, options));
          
        this.$items = $('<div/>');
        this.$items.attr("class", "ui-menu-items");

        this.itemsHash = {};

        this.on('render', function(){
            this.applyModelValueToDisplay();
        });

        if(this.options.closed){
            this.isOpen = false;
        }else{
            this.isOpen = true;
        }

        if(!this.options.static && this.options.searchable){
            this.searchBox = new ui.Input({
                classes:"search-box",
                placeholder:"Search...",
                iconprefix:'icon icon-search'
            });

            this.searchIndex = lunr(function () {
                this.ref('value');
                this.field('label');
            })

        }

    },

    focus:function(){
        if(this.searchBox){
            this.searchBox.focus();
        }else{
            this._super();
        }
    },


    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    toggleOpen:function(){
        if(this.isOpen){
            this.close();
        }else{
            this.open();
        }
    },
    open:function(animated){
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.removeClass('ui-closed');
        this.$el.addClass('ui-open');
        this.isOpen = true;

        if(this.searchBox){
            this.searchBox.model.set('value', '');
        }

        var h = this.$items.outerHeight();

        if(this.searchBox){
            h += this.searchBox.$el.outerHeight();
            _.delay(_.bind(this.focus,this), 100);
        }
        this.$el.height(h);
        //_.delay(_.bind(this.focus,this), 100);
        _.delay(_.bind(function(){
            this.$el.toggleClass('ui-animated', false);
            this.$el.css('height', 'auto');
        },this), 250);

 

    },

    close:function(animated){
        this.isOpen = false;
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.addClass('ui-closed');
        this.$el.removeClass('ui-open');
        this.$el.height('0px');
    },

    render:function(){

        this._super(false);

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        if(this.searchBox){
            this.$el.append(this.searchBox.render().$el);
        }

        //render items
        this.renderItems();

        if(this.isOpen == false){
            this.close(false);
        }

        this.trigger('render');

        return this;
    },

    renderItems:function(){
        this.$items.empty();
        this.itemsHash = {};
        if(this.searchIndex && this.searchIndex.reset){
            this.searchIndex.reset();
        }

        _.each(this.options.items, function(itemObject, i){


            if(_.isString(itemObject)){
                var str = itemObject;
                itemObject = {
                    label:str,
                    value:str
                }
            }
      
            if(this.searchIndex){
                this.searchIndex.add(itemObject);
            }
      
            var button = new ui.Button({
                classes:"ui-menu-item no-style",
                label:itemObject.label,
            }).render();

            if(!this.options.static){
                this.listenTo(button, 'action', function(){
                    this.model.set('value', itemObject.value);
                    this.trigger('select', {value:itemObject.value, button:button});
                }); 
            }

            this.listenTo(button, 'mousedown', function(event){
                event.preventDefault();
                event.stopPropagation();
                return false;
            });



            this.$items.append(button.$el);
            this.itemsHash[itemObject.value.toString()] = button;

        },this);

        this.$el.append(this.$items);

    },


    setDisplayValue:function(value){
        if(typeof value == 'undefined'){
            value = '';
        }
        value = value.toString();
        _.each(this.itemsHash, function(item, key){
            if(key == value){
                item.active(true);
            }else{
                item.active(false);
            }
        });
    },
    // getDisplayValue:function(){
    //     return this.$input.val();
    // },

    filterItems:function(){
        
        if(this.searchBox.model.get('value') == ''){
            _.each(this.itemsHash, function(itemObject){
                itemObject.$el.show();
            },this);
            
        }else{
            _.each(this.itemsHash, function(itemObject){
                itemObject.$el.hide();
            },this);
            _.each(this.searchIndex.search(this.searchBox.model.get('value')), function(refObject){

                if(this.itemsHash[refObject.ref]){
                    this.itemsHash[refObject.ref].$el.show();
                }

            },this);
        }






    },

    listenToDisplay:function(){
        
        this._super();

        if(this.searchBox){
            this.listenTo(this.searchBox.model, 'change', this.filterItems);
        }

        $('body').on('keydown', '#'+this.id, _.bind(function(event){
            if(event.keyCode == 38){
                this.cycleUp();
            }else if(event.keyCode == 40){
                this.cycleDown();
            }
            if(event.keyCode == 38 || event.keyCode == 40){
                event.preventDefault();
            }
        },this));


    },
    cycle:function(direction){
        var $nextFocus = null;
        var $currentFocus = null;
        $currentFocus = this.$el.find('.ui-menu-item:focus');

        if($currentFocus.length){
            if(direction == 'down'){
                $nextFocus = $currentFocus.next();
            }else if(direction == 'up'){
                $nextFocus = $currentFocus.prev();
            }
        }else{
            $nextFocus = this.$el.find('.ui-menu-item').first();
        }

        if($nextFocus != null){
            $nextFocus.focus();
        }

    },
    cycleDown:function(){
        this.cycle('down');
    },
    cycleUp:function(){
        this.cycle('up');
    },

    destroy:function(){
        this._super();
        _.each(this.itemsHash, function(item){
            item.destroy();
        });
    }

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new UIMenu(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        var elementFromHTML = new UIMenu(options).render();
    }

});
  
ui.registerHelper('ui-select', _.bind(UIMenu.templateHelper, UIMenu)); 

module.exports = UIMenu;






