var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');
var ui = require('./ui');

var FocusTest = Base.extend({

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-focus-test',
        }, options));

        this.child = new ui.FocusTestChild();
        this.child.render().$el.hide();

        this.child.on('ui-focusout', _.bind(function(){
            this.child.$el.hide();
        },this))

        var testInputId = _.uniqueId();
        this.$testInput = $('<input id="'+testInputId+'"/>');

        $('body').on('focusin', '#'+testInputId, _.bind(function(){
            this.child.render().$el.show();
        },this));


        // this.on('focusout', function(e){
            
        // })

    },

    render:function(){

        this._super();

        this.$el.append(this.$testInput);
        this.$el.append(this.child.$el);

        return this;

    },

    focusin:function(trigger){
        //this._super(event);
        //console.log('focusin')
        console.log('focusin.local')
        this._super();
        this.$testInput.focus();

    },

    focusout:function(trigger){
        console.log('focusout.local')
        this._super();
        this.child.$el.hide();
    }


});
 
module.exports = FocusTest;






