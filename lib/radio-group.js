var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');
var ui = require('./ui');

var UIRadioGroup = Base.extend({

    UIType:"radio-group",

    initialize:function(options){

        this._super(_.extend({
            className:'RadioGroup',
            defaultValue:"",
            items:[] 
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.$items = $('<div/>');
        this.$items.attr('class', 'ui-radio-group-items');
        this.itemsHash = {};

        this.$label = $('<label/>');
        this.$label.attr('for', this.name);
        this.$label.attr('data-name', this.name);

        if(this.options.description){
            this.$description = $('<div class="ui-description"></div>');
        }

        if(this.options.closed){
            this.isOpen = false;
        }else{
            this.isOpen = true;
        }

        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });

    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    toggleOpen:function(){
        if(this.isOpen){
            this.close();
        }else{
            this.open();
        }
    },

    open:function(animated){
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.removeClass('ui-closed');
        this.$el.addClass('ui-open');
        this.isOpen = true;
        this.$el.height(this.$items.outerHeight());
        _.delay(_.bind(this.focus,this), 100);
    },

    close:function(animated){
        this.isOpen = false;
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.addClass('ui-closed');
        this.$el.removeClass('ui-open');
        this.$el.height('0px');
    },

    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-radio-group');
        this.$el.addClass(this.options.classes);
        if(typeof this.options.form == "string" && this.options.form != "" ){
            this.$el.attr("data-form", this.options.form);
        }
        if(this.options.required){
            this.$el.attr('data-ui-required', '');
            this.$el.addClass('ui-required');
        }
        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //append description
        if(this.options.description){
            this.$el.append(this.$description);
            this.$description.html(this.options.description);
        }

        //render items
        this.renderItems();

        if(this.isOpen == false){
            this.close(false);
        }

        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }


        if(!this.options.static){
            this.trigger('render');
            this.listenToDisplay();
        }else{
            this.setDisplayValue(this.options.value);
        }

        return this;
    },

    renderItems:function(){
        this.$items.empty();
        this.itemsHash = {};
        _.each(this.options.items, function(itemObj, i){

            var toggle = new ui.Toggle({
                classes:"ui-radio-group-item ui-child",
                label:itemObj.label,
                style:"radio",
                name:this.name,
                value:itemObj.value,
                static:this.options.static || ui.static
            }).render();

            if(!this.options.static){ 
                this.listenTo(toggle, 'activate', function(){
                    this.model.set('value', itemObj.value);
                    this.trigger('select', {value:itemObj.value, toggle:toggle});
                }); 
            }

            this.$items.append(toggle.$el);
            this.itemsHash[itemObj.value.toString()] = toggle;

        },this);

        if(this.options.other){

            this.options.other.placeholder = this.options.other.placeholder || "Other";

            var toggle = new ui.Toggle({
                classes:"ui-radio-group-item ui-child",
                style:"radio",
                name:this.name,
                value:'other',
                static:this.options.static || ui.static,
                inputacc:_.extend({
                    placeholder:this.options.other.placeholder,
                    model:this.model,
                    attribute:"other"
                },this.options.other)
            }).render();

            if(!this.options.static){ 
                this.listenTo(toggle, 'activate', function(){
                    this.model.set('value', 'other');
                    this.trigger('select', {value:'other', toggle:toggle});
                }); 
            }

            this.$items.append(toggle.$el);
            this.itemsHash['other'] = toggle;
        } 

        this.$el.append(this.$items);

    },


    displayError:function(isError, message){
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
            this.$el.removeClass('ui-error'); 
            this.$el.removeClass('ui-error-show');
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$items.before(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }
        if(isError){
            this.$errorMessage.html(message);
            this.$items.before(this.$errorMessage);
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },


    setDisplayValue:function(value){
        value = value || "";
        value = value.toString();
        _.each(this.itemsHash, function(item, key){
            if(key == value){
                item.active(true);
            }else{
                item.active(false);
            }
        });
    },
    // getDisplayValue:function(){
    //     return this.$input.val();
    // },

    listenToDisplay:function(){
        this.$el.addClass("ui-init");
        ui.$body.on('focusin focus', '#'+this.id, _.bind(this.focusin, this));
        ui.$body.on('focusout', '#'+this.id, _.bind(this.focusout, this));
    },



    focus:function(event){
        $('#'+this.id).focus();
    },
    focusin:function(){
        this.$el.addClass('ui-focus');
    },
    focusout:function(event){
        this.$el.removeClass('ui-focus');
        if(this.options.closed){
            this.close();
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UIInput(data.hash).render().$el);
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.children('label').length){
            options.label = $element.children('label').text();
        }

        options.items = [];
        $element.find('input[type="radio"]').each(function(index, optionEl){

            options.items.push({
                label:$(optionEl).attr('data-label'),
                value:optionEl.value
            });
            if(optionEl.checked == true){
                options.value = optionEl.value;
            }

        });
        
        if($element.attr('data-ui-attribute') != ''){
            options.name = $element.attr('data-ui-attribute');
        }

        var elementFromHTML = new UIRadioGroup(options).render();
    }




});

ui.registerHelper('ui-radio-group', _.bind(UIRadioGroup.templateHelper, UIRadioGroup)); 

module.exports = UIRadioGroup;






