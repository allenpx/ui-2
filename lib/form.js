var Backbone = require('backbone');
var $ = require('jquery');
var _ = require('underscore');

var ui = require('./ui');

var UIForm = Backbone.View.extend({

    initialize:function(options){
        this.options = _.extend({

        }, options || {});

        this.options.name = this.options.name || _.uniqueId('uiform');

        if(this.options.formTagId){
            this.form = $('#'+this.options.formTagId);
        }

        if(this.options.model){
            if(_.isString(this.options.model)){
                this.model = ui.models[this.options.model]
            }else{
                this.model = this.options.model;
            }
        }

        ui.registerForm(this.options.name, this);

    }

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new Input(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){

        var options = {};

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        options.name = $element.find('input').attr('name');
        options.value = $element.find('input').val();

        var elementFromHTML = new UIForm(options).render();
    }

});

ui.registerSelector('.ui-form[data-ui]:not(.ui-init)', _.bind(UIForm.newFromHTML, UIForm));

module.exports = UIForm;