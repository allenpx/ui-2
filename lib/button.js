var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');
var ui = require('./ui');

var UIButton = Base.extend({

    UIType:"button",

    tagName:'button',

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-button',
            defaultValue:"",
            className:"Button",
            label:"Button",
            type:"button",
            internalModel:false
        }, options));

        this.$label = $('<span/>');

        // if(this.options.static && this.options.type == "submit"){
        //     console.log(this.options.model)
        // }
    
    },



    render:function(){

        this._super(false);

        this.$el.attr("type", this.options.type);

        var classes = [];

        if(this.options.disabled){
            this.disable();
        }
        if(this.options.iconleft){
            classes.push('has-icon-left');
            var iconLeft = $('<i class="'+this.options.iconleft+'"></i>');
            this.$el.append(iconLeft);
        }

        if(this.options.icon){
            classes.push('has-icon');
            var iconCenter = $('<i class="'+this.options.icon+'"></i>');
            this.$el.append(iconCenter);
        }

        //append label
        if(this.options.label && this.options.label != '' && this.options.label != null){
            this.$label.text(this.options.label);
            this.$el.append(this.$label);
        }

        if(this.options.iconright){
            classes.push('has-icon-right');
            var iconRight = $('<i class="'+this.options.iconright+'"></i>');
            this.$el.append(iconRight);
        }

        this.$el.addClass(classes.join(' '));

        if(!this.options.static){
            this.trigger('render');
        }

        return this;

    },


    listenToDisplay:function(){

        this._super();

        if(this.options.menu && typeof this.menu == 'undefined'){
            this.options.menu.closed = true; 
            this.options.menu.floatParent = this;
            this.menu = new ui.Menu(this.options.menu);
            this.listenTo(this.menu, 'select', function(){
                this.menu.close();
            });

            ui.$floatingLayer.append(this.menu.render().$el);
            this.options.externalElements = [this.menu];
        }

        this.$el.on('click', _.bind(this.action, this));
        this.$el.on('mousedown', _.bind(this.onMouseDown, this));
    },

    onMouseDown:function(event){

        event.preventDefault();
        this.trigger('mousedown', event);

    },

    action:function(event){

        if(event){
            event.preventDefault();
        }

        if(this.options.action){
            this.options.action();
        }
 
        if(this.options.type == "submit" && this.options.model){
            this.options.model.submit();
        }

        this.trigger('action');

    },

    focusin:function(){
        this._super();
        if(this.menu){
            this.menu.positionToFloatParent();
            this.menu.$el.width(this.$el.width());
            this.menu.open();
            this.menu.focus();
        }
    },

    focusout:function(){
        this._super();
        if(this.menu){
            this.menu.close();
        }
    },

    _active:false,
    active:function(bool){
        if(bool == true){
            this._active = true;
            this.$el.addClass('ui-active');
        }else if(bool == false){
            this._active = false;
            this.$el.removeClass('ui-active');
        }
        return this._active;
    },

    disable:function(){
        this.$el.attr('disabled', '');
    },

    enable:function(){
        this.$el.removeAttr('disabled');
    },


}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UIButton(data.hash).render().$el);
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        options.label = $element.find('span').text();
        options.type = $element.attr('type') || 'button';

        var elementFromHTML = new UIButton(options).render();
    },

});
 
//ui.registerSelector('.ui-button[data-ui]:not(.ui-init)', _.bind(Input.newFromHTML, Input));
ui.registerHelper('ui-button', _.bind(UIButton.templateHelper, UIButton)); 

module.exports = UIButton;






