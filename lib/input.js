var ui = require('./ui');
var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');

var UIInput = Base.extend({

    UIType:"input",

    initialize:function(options){

        this._super(_.extend({
            _options:options,
            baseClass:'ui-input',
            className:'Input',
            defaultValue:"",
            type:"text",
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.inputId = 'input_'+this.id;
        this.$input = $('<input/>');
        this.$input.attr('id', this.inputId);

        if(this.options.type == "password"){
            this.$input.attr('type', 'password');
        }else if(this.options.type != 'date'){
            this.$input.attr('type', this.options.type);
        }

        if(this.options.type == "date"){
            this.options.format = this.options.format || 'MM/DD/YYYY';
        }

        if(this.options.type == "date" && typeof this.options.placeholder == 'undefined'){
            this.options.placeholder = this.options.format;
        }

        
        this.$input.attr('name', this.name);

        this.$label = $('<label/>');
        this.$label.attr('for', this.inputId);

        if(this.options.readonly){
            this.$input.attr('readonly', true);
        }
        if(this.options.placeholder){
            this.$input.attr('placeholder', this.options.placeholder);
        }
        if(this.options.autocomplete){
            this.$input.attr('autocomplete', true);
        }

        if(this.options.error){
            this.options.errorValue = this.options.value;
        }

        
        // if(this.options.autoselect){
        //     this.$input.attr('onClick', 'this.setSelectionRange(0, this.value.length)');
        // }


    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }

    }, 



    render:function(){

        this._super(false);

        if(this.options.required){
            this.$el.attr('data-ui-required', '');
            this.$el.addClass('ui-required');
        }
        if(this.staticModel){
            this.$el.attr('data-ui-model', this.staticModel);
        }

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //append input
        this.$el.append(this.$input);

        if(this.options.iconprefix){
            classes.push('ui-has-acc-left');
            var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
            this.attachAccessory(iconLeft);
            this.$input.before(iconLeft);
        }

        if(this.options.iconsuffix){
            classes.push('ui-has-acc-right');
            var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
            this.attachAccessory(iconRight);
            this.$input.after(iconRight);
        }

        if(this.options.prefix && _.isString(this.options.prefix)){
            classes.push('ui-has-acc-left');
            var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
            this.attachAccessory(prefix);
            this.$input.before(prefix);
        }
        if(this.options.suffix && _.isString(this.options.suffix)){
            classes.push('ui-has-acc-right');
            var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
            this.attachAccessory(suffix);
            this.$input.after(suffix);
        }


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            if(this.options.suffix.UIElement && this.options.suffix.UIType == "select"){
                var suffix = $('<div class="ui-acc ui-acc-select ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.render().$el);
            this.$input.after(suffix);
        }


        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }

        this.$el.addClass(classes.join(' '));

        if(!this.options.static){
            this.trigger('render');
        }else{
            this.setDisplayValue(this.options.value);
        }

        if(this.options.error){
            this.error(this.options.error);
        }

        return this;
    },
 

    setDisplayValue:function(value){
        if(this.$input){
            this.$input.val(value);
            this.$input.attr('value',value);
        }
    },

    getDisplayValue:function(){
        return this.$input.val();
    },

    listenToDisplay:function(){

        this._super();

        $('body').on('change keyup', '#'+this.inputId, _.bind(function(event){
            this.dirty();
            this.applyDisplayValueToModel();
        }, this));

        if(this.options.type == "date" && typeof this.calendar == 'undefined'){
            this.calendar = new ui.Calendar({
                classes:'ui-popup',
                floatParent:this
            });
            ui.$floatingLayer.append(this.calendar.render().$el);
            this.options.externalElements = [this.calendar];
            // $('body').on('mousedown mouseup click focusin', '#'+this.calendar.id, _.bind(function(event){
            //     event.stopPropagation();
            //     console.log('test')
            //     _.defer(_.bind(function(){
            //         this.focus();
            //     },this) );
            // },this));
        }



    },

    displayError:function(isError, message){
        
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
            this.flashError();
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$el.append(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }

        if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },

    focus:function(event){
        this.$input.focus();
    },

    focusin:function(){
        //console.log('focusin')
        this._super();
        
    },

    focusout:function(event){
        this._super();

    }, 

    attachAccessory:function($element){
        if(process.browser){
            var id = _.uniqueId('acc');
            $element.attr('id', id);

            $('body').on('click', '#'+id, $.proxy(this.focus, this));
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UIInput(data.hash).render().$el);
    },

});

ui.registerHelper('ui-input', _.bind(UIInput.templateHelper, UIInput)); 

module.exports = UIInput;






