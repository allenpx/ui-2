'use strict';

var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');

var ui = require('./ui');

var UISelect = Base.extend({

    UIType:"select",

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-select',
            className:"Select",
            defaultValue:"",
            items:[],
            labelAttribute:'label',
            valueAttribute:'value',
        }, options));

        if(this.options.placeholder){
            this.options.items.unshift({value:'', label:this.options.placeholder})
        }
          
        this.$errorMessage = $('<div class="ui-error-message"></div>');

        this.selectId = 'select_'+this.id;
        this.displayId = 'display_'+this.id;
        this.$select = $('<select/>');
        this.$select.attr("class", 'ui-display');
        this.$select.attr("id", this.selectId);
        this.$select.attr("name", this.name);

        if(!this.options.static){
            this.$display = $('<div class="ui-display" tabindex="0"/>');
            this.$display.attr("id", this.displayId);
        }

        this.$menuEl = $('<div/>');

        this.$label = $('<label/>');
        this.$label.attr('for', this.selectId);

        this.$icon = $('<i class="ui-select-icon icon icon-chevron-down"></i>');

        if(this.options.error){
            this.options.errorValue = this.options.value;
        }

        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });



    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    initInternalModel:function(){

        this._super();

        this.menu = new ui.Menu({
            classes:'ui-select-menu',
            el:this.$menuEl,
            model:this.model,
            attribute:"value",
            items:this.options.items,
            closed:true,
            searchable:this.options.searchable,
            floatParent:this
        });

        this.listenTo(this.menu, 'focusout', function(){
            if(!this.hasFocus()){
                this.focusout();
            }
        });

        this.listenTo(this.menu, 'select', function(){
            this.menu.close();
            //$(':focus').blur();
        });

        // this.listenTo(this.menu, 'focusin', function(){
        //     this.$el.addClass('ui-focus');
        // });

        // this.listenTo(this.menu, 'focusout', function(){
        //     this.$el.removeClass('ui-focus');
        // });

    },

    render:function(){

        this._super(false);

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //render select
        this.$el.append(this.renderSelect()); 

        if(this.options.iconprefix){
            classes.push('ui-has-acc-left');
            var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
            this.attachAccessory(iconLeft);
            this.$select.before(iconLeft);
        }

        if(this.options.iconsuffix){
            classes.push('ui-has-acc-right');
            var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
            this.attachAccessory(iconRight);
            this.$select.after(iconRight);
        }

        if(this.options.prefix && _.isString(this.options.prefix)){
            classes.push('ui-has-acc-left');
            var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
            this.attachAccessory(prefix);
            this.$select.before(prefix);
        }
        if(this.options.suffix && _.isString(this.options.suffix)){
            classes.push('ui-has-acc-right');
            var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
            this.attachAccessory(suffix);
            this.$select.after(suffix);
        }


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.$el);
            this.$select.after(suffix);
        }

        if( (this.options.errorMessage || this.options.errormessage) && this.options.errorMessage ){
            this.error(this.options.errorMessage);
        }

        this.$el.addClass(classes.join(' '));

        this.$el.append(this.$icon);
        

        if(!this.options.static){
            this.$select.after(this.$display);
            this.$select.hide();
            this.trigger('render');
            ui.$floatingLayer.append(this.menu.render().$el);
            this.options.externalElements = [this.menu];
        }else{
            this.setDisplayValue(this.options.value);
        }

        return this;
    },

    renderSelect:function(){
        this.$select.empty();

        _.each(this.options.items, function(itemObj, index){
            if(_.isString(itemObj)){
                var str = itemObj;
                itemObj = {
                    label: str,
                    value: str
                }
            }

            var $option = $('<option>'+itemObj[this.options.labelAttribute]+'</option>');
            $option.attr('value', itemObj[this.options.valueAttribute]);
            this.$select.append($option);

        },this);

        return this.$select;

    },
 
    afterRender:function(){
        this._super();
        if(!this.options.static){
            this.listenToDisplay();
        }
        if(this.options.error){
            this.error(this.options.error);
        }
    },

    setDisplayValue:function(value){

        if(this.$select){
            //this.$select.val(value);
            this.$select.attr('value',value);
            //this.$select[0].value = value;
            var $selectedOption = this.$select.find('option[value="'+value+'"]');
            if($selectedOption.length){
                $selectedOption.attr('selected', '');
                if(this.$display){
                    this.$display.text($selectedOption.text());
                }
            }
        }

        if(value == ''){
            this.$el.addClass('ui-empty-value');
        }else{
            this.$el.removeClass('ui-empty-value');
        }

    },
    getDisplayValue:function(){
        return this.$select.val();
    },

    listenToDisplay:function(){

        this._super();

        ui.$body.on('change.'+this.id+' keyup.'+this.id, '#'+this.selectId, _.bind(function(event){
            this.applyDisplayValueToModel();
        }, this));

        var self = this;
        // ui.$body.on('mousedown', '#'+this.selectId, function(event){
        //     event.preventDefault();
        //     event.stopPropagation();
        //     return false;
        // });
        // ui.$body.on('click', '#'+this.displayId, _.bind(function(event){
        //     console.log('click')
        //     if(!this.isFocused){
        //         this.menu.close();
        //     }
        // }, this));

        ui.$body.on('keydown.'+this.id, '#'+this.selectId, function(event){
            var code = event.keyCode || event.which;
            if(code == 13) { //Enter keycode
                event.preventDefault();
                event.stopPropagation();
                //self.menu.toggleOpen();
                return false;
            }
        });

        this.listenTo(this.menu, 'focusout', function(){
            //this.$el.blur();
        })
        //ui.$body.on('focusin', '#'+this.selectId, _.bind(this.focusin, this));
        //ui.$body.on('focusout', '#'+this.selectId, _.bind(this.focusout, this));

    },

    displayError:function(isError, message){

        if(this.options.error && this.model.get('value') == this.options.errorValue){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
            return false;
        }else{
            this.options.error = null;
        }

        if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
        }else{
            this.$el.removeClass('error');
            if(this.$errorMessage){
                this.$errorMessage.remove(); 
            }
        }
    },

    focus:function(){
        this.$display.focus();
    },

    focusin:function(){
        this._super();
        if(this.menu){
            this.menu.positionToFloatParent();
            this.menu.$el.width(this.$el.width());
            this.menu.open();
        }

        var self = this;
        self.$display.off('click');
        self.$display.one('click', function(){
            
            self.$display.on('click', function(){
                self.menu.toggleOpen();
            })
        })
    },

    focusout:function(){
        this._super();
        if(this.menu){
            this.menu.close(); 
        }
        this.$display.off('mouseup');
        this.$display.off('click');
        this.validateModel();
    },

    attachAccessory:function($element){
        if(process.browser){
            var id = _.uniqueId('acc');
            $element.attr('id', id);

            ui.$body.on('click', '#'+id, $.proxy(this.focus, this));
        }
    },

    destroy:function(){
        this._super();
        if(typeof this.menu != 'undefined'){
            this.menu.destroy();
            this.menu.$el.remove();
        }
        this.$display.off();
    }

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new UISelect(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        options.items = [];
        $element.find('select option').each(function(index, optionEl){
            options.items.push({
                label:optionEl.text,
                value:optionEl.value
            });
            if(optionEl.value == ''){ 
                options.placeholder = optionEl.text;
            }
        });

        options.name = $element.find('select').attr('name');
        options.value = $element.find('select').val();

        var elementFromHTML = new UISelect(options).render();
    }

});
  
ui.registerHelper('ui-select', _.bind(UISelect.templateHelper, UISelect)); 

module.exports = UISelect;






