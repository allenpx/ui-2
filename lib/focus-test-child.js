var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');
var ui = require('./ui');

var FocusTestChild = Base.extend({

    initialize:function(options){

        this._super(_.extend({
            baseClass:'ui-focus-test',
        }, options));

    },

    render:function(){

        this._super();

        this.$el.append('<h3>Child</h3>');
        this.$el.append('<input/>');
        this.$el.append('<input/>');
        this.$el.append('<input type="button"/>');
        this.$el.append('<input type="button"/>');
        return this;

    }


});
 
module.exports = FocusTestChild;






