var Base = require('./base');
var ui = require('./ui');
var $ = require('jquery');
var _ = require('underscore');


var Dialog = Base.extend({

    model:null,
    contentIsView:false,
    $content:null,
    $container:null,
    initialize:function(options){
        this._super(options);

        this._super(_.extend({
            classes:"ui ui-dialog"
        }, options));

        this.app = this.options.app || null;

        this.$containerOuter = $('<div class="container-outer"></div>');
        this.$overlay = $('<div class="overlay"></div>');
        this.$containerInner = $('<div class="container-inner"></div>');
        this.$contentOuter = $('<div class="content-outer"></div>');
        this.$contentInner = $('<div class="content-inner"></div>');
        this.$close = $('<div class="close"><i class="icon icon-close"></i></div>');
        this.$content = $('<div class="content"></div>');

        this.$loadingIcon = $('<div class="loading-container"><div class="loading-icon"></div></div>');

        this.$headerTitle = $('<div class="title"></div>');
        this.$header = $('<div class="header"></div>'); 
        this.$header.append(this.$headerTitle);
        this.$footer = $('<div class="footer"></div>');
 
        $(window).on('resize.modal', $.proxy(this.resize,this));

        this.$overlay.click($.proxy(function(){
            this.hide();
        },this));
        this.$close.click($.proxy(function(){
            this.hide();
        },this));

        this.resize();

    },
    render:function(){
        this.$el.empty();
        this.$el.addClass('ui ui-dialog');
        this.$el.addClass(this.options.classes);

        this.$containerOuter.append(this.$overlay);
        this.$containerOuter.append(this.$containerInner);
        this.$containerInner.append(this.$contentOuter);
        this.$contentOuter.append(this.$contentInner);
        this.$containerInner.prepend(this.$header);
        this.$containerInner.append(this.$close);
        this.$contentInner.append(this.$content);
        this.$containerInner.append(this.$footer);

        if(this.options.title == undefined && this.options.content.title == undefined){
            this.options.title = '&nbsp;';
        }else if(this.options.content.title != undefined){
            this.$el.addClass('has-title')
            this.options.title = this.options.content.title;
        }
        this.$headerTitle.html(this.options.title);
        this.$footer.empty();

        this.options.options = this.options.content.modalOptions || this.options.options;

        this.setOptions(this.options.options);

        if(this.options.contentPadding == false){
            this.$content.addClass('collapse');
        }
        if(this.options.fill == true){
            this.$content.addClass('fill');
        }

        this.$el.append(this.$containerOuter);

        this.setContent(this.options.content);



        return this;

    },
    setContent:function(content){

        this.contentIsView = false;
        this.$content.empty();
        if(_.isString(content)){
            this.$content.append(content);
        }else if(_.isFunction(content)){
            var renderedContent = content();
            if(typeof renderedContent.$el != 'undefined'){
                this.contentIsView = true;
                this.$content.append(renderedContent.$el);
            }else{
                this.$content.append(renderedContent);
            }
        }else if(typeof content.render != 'undefined'){

            this.contentIsView = true;
            this.$content.append(content.render().$el);
            content.modal = this;
        }else{ 
            this.$content.append(content);
        } 
    },
    show:function(){
        _.defer($.proxy(function(){
            this.$el.addClass('active');
            if(this.options.onShow){
                this.options.onShow(this);
            }
            this.trigger('show');
        },this));
    },
    hide:function(){
        this.$el.addClass('hidden');
        this.trigger('hide');
        var self = this;
        if(this.options.onClose){
            this.options.onClose(this);
        }
        _.delay(function(){
            self.destroy();
        },250); 
    },
    cancel:function(){
        this.trigger('cancel');
        if(this.contentIsView && typeof this.options.content.cancel != 'undefined'){
            this.options.content.cancel();
        }
        this.hide();
    },
    setOptions:function(options){
        this.$footer.empty();
        if(options){
            this.$el.addClass('has-options');
            if(this.options.optionsAlign == "center"){
                this.$footer.addClass('text-center');
            }else if(this.options.optionsAlign == "right"){
                this.$footer.addClass('text-right');
            }
            _.each(options, function(optionObject){
                var $button = $('<div class="button">'+optionObject.label+'</div>');
                if(optionObject.action == "cancel"){
                    $button.addClass('cancel');
                    $button.on('click', $.proxy(this.cancel,this));
                }else if(optionObject.action != undefined && typeof optionObject.action == "string"){
                    $button.on('click', $.proxy(function(){
                        this.options.content.trigger(optionObject.action);
                    },this));
                    
                }else if(optionObject.action != undefined && typeof optionObject.action == "function"){
                    $button.on('click', $.proxy(function(){
                        optionObject.action();
                    },this));
                }
                this.$footer.append($button);
            },this);
        };
    },
    destroy:function(){
        this._super();
        this.$el.remove();
        $(window).off('resize.dialog');
        this.trigger('destroy', {dialog:this});
    },
    resize:function(){
        if($(window).width() >= 768 && this.options.width){
            this.$containerInner.css('max-width', (typeof this.options.width == 'number')?this.options.width+"px":this.options.width );
        }else{
            this.$containerInner.css('max-width', "");
        }
    },
    enable:function(){
        this.$el.removeClass('disabled');
    },
    disable:function(){
        this.$el.addClass('disabled');
    },

    _isLoading:false,
    isLoading:function(bool){
        if(bool){
            this.$containerInner.append(this.$loadingIcon);
            this.$containerOuter.addClass('loading');
        }else{
            this.$loadingIcon.remove();
            this.$containerOuter.removeClass('loading');
        }
    }

},{

    built:false,

    $container:null,

    currentDialog:null,

    dialogs:[],

    buildContainer:function(){
        Dialog.$container = $('<div class="ui ui-dialogs"></div>');
        Dialog.$container.append('<div class="overlay"></div>');
        $('body').append(Dialog.$container);
    },

    push:function(dialogObject){

        ui.trigger('dialog.push');

        if(typeof dialogObject == 'undefined'){
            Dialog.hide();
            return false;
        }

        if(Dialog.built == false){
            Dialog.buildContainer();
            Dialog.built = true;
        }

        if(Dialog.dialogs.length){
            Dialog.dialogs[0].hide();
        }

        Dialog.show();
        
        var newDialog = null;
        if(typeof dialogObject.$el == 'undefined'){
            newDialog = new Dialog(dialogObject).render();
        }else{
            newDialog = dialogObject;
        }

        Dialog.currentDialog = newDialog;
        Dialog.dialogs.push(newDialog);
        Dialog.$container.append(Dialog.currentDialog.$el);
        Dialog.currentDialog.show();
        Dialog.currentDialog.on('hide', Dialog.onhide, Dialog);
        Dialog.currentDialog.on('destroy', Dialog.destroy, Dialog);


    },
    show:function(){

        ui.trigger('dialog.show');

        Dialog.$container.addClass('active');
        $('html,body').addClass('dialog-open');
    },
    hide:function(){
        Dialog.currentDialog.hide();
        $('html,body').removeClass('dialog-open');
    },
    onhide:function(){

        ui.trigger('dialog.hide');

        $('html,body').removeClass('dialog-open');
    },
    destroy:function(event){



        event.dialog.off('hide');
        event.dialog.off('destroy');
        Dialog.dialogs.shift();
        if(Dialog.dialogs.length == 0){
            Dialog.$container.removeClass('active');
        }
    }

});

module.exports = Dialog;