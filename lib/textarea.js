var ui = require('./ui');
var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');

var UITextArea = Base.extend({

    UIType:"textarea",

    initialize:function(options){
        
        this._super(_.extend({
            baseClass:'ui-textarea',
            className:'TextArea',
            defaultValue:"",
            type:"text",
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.inputId = 'input_'+this.id;
        this.$input = $('<textarea></textarea>');
        this.$input.attr('id', this.inputId);

        this.$input.attr('name', this.name);

        this.$label = $('<label/>');
        this.$label.attr('for', this.inputId);

        if(this.options.readonly){
            this.$input.attr('readonly', true);
        }

        if(this.options.autocomplete){
            this.$input.attr('autocomplete', true);
        }

        if(this.options.error){
            this.options.errorValue = this.options.value;
        }


    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }

    }, 



    render:function(){

        this._super();

        if(this.options.required){
            this.$el.attr('data-ui-required', '');
            this.$el.addClass('ui-required');
        }
        if(this.staticModel){
            this.$el.attr('data-ui-model', this.staticModel);
        }

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //append input
        this.$el.append(this.$input);


        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }

        this.$el.addClass(classes.join(' '));

        if(!this.options.static){
            this.trigger('render');
            this.listenToDisplay();
        }else{
            this.setDisplayValue(this.options.value);
        }

        if(this.options.error){
            this.error(this.options.error);
        }

        return this;
    },
 

    setDisplayValue:function(value){
        if(this.$input){
            this.$input.val(value);
            this.$input.attr('value',value);
        }
    },

    getDisplayValue:function(){
        return this.$input.val();
    },

    listenToDisplay:function(){
        
        this._super();

        $('body').on('change keyup', '#'+this.inputId, _.bind(function(event){
            this.dirty();
            this.applyDisplayValueToModel();
        }, this));

    },

    displayError:function(isError, message){
        
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
            this.flashError();
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$el.append(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }

        if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },

    focus:function(event){
        this.$input.focus();
    },

    attachAccessory:function($element){
        if(process.browser){
            var id = _.uniqueId('acc');
            $element.attr('id', id);

            $('body').on('click', '#'+id, $.proxy(this.focus, this));
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UITextArea(data.hash).render().$el);
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        options.name = $element.find('textarea').attr('name');
        options.placeholder = $element.find('textarea').attr('placeholder');
        
        if($element.attr('data-ui-required')){
            options.required = true;
        }
        options.value = $element.find('textarea').val();

        var elementFromHTML = new UITextArea(options).render();
    }

});

ui.registerHelper('ui-textarea', _.bind(UITextArea.templateHelper, UITextArea)); 

module.exports = UITextArea;






