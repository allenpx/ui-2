var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var moment = require('moment');
if(!process.browser){
    var clndr = function(){};
}else{
    var clndr = require('clndr');
}
var ui = require('./ui');

var CalendarMonthViewTemplate = require('./hbs/calendar-month-view');

var UICalendar = (require('./base')).extend({

    UIType:"Calendar",

    initialize:function(options){
        
        this.options = _.extend({
            baseClass:'ui-calendar',
            className:'Calendar',
            daySelectAttribute:'startDate'
        }, options);

        this.id = _.uniqueId();

        $(this.el).attr('id', this.id);

        this.model = new Backbone.Model({
            startDate:null,
            endDate:null
        });
        
        var self = this;
        this.$toolbar = $('<div class="ui-calendar-toolbar"></div>');
        this.$title = $('<div class="title"></div>');
        this.$prevButton = $('<div class="prev-button"><i class="icon icon-chevron-left"></i></div>');
        this.$nextButton = $('<div class="next-button"><i class="icon icon-chevron-right"></i></div>');
        this.$toolbar.append(this.$title);
        this.$toolbar.append(this.$prevButton);
        this.$toolbar.append(this.$nextButton);
        this.$calendar = $('<div class="ui-calendar-month-view"></div>');
        this.calendar = this.$calendar.clndr({
            clickEvents: {
                click: function(target) {
                    if( !$(target.element).hasClass('inactive') ) {
                        self.model.set(self.options.daySelectAttribute, target.date);
                    } else {
                        console.log('That date is outside of the range.');
                    }
                }
            },
            click: function(target){
                console.log(target);
            },
            onMonthChange: function(month) {

            },
            render:_.bind(function(data){
                self.$title.text(data.month + " " + data.year);

                return CalendarMonthViewTemplate(data);
            },this),
            doneRendering: _.bind(function(){
                this.highlightDay();
            },this)
        });

        this.listenTo(this.model, 'change', function(){

            if(this.model.get(this.options.daySelectAttribute) != null && this.calendar.month.month() != this.model.get(this.options.daySelectAttribute).month()){
                self.calendar.setMonth(this.model.get(this.options.daySelectAttribute).month());
            }
            self.highlightDay();

        });

        window.calendar = this.calendar;
    },

    highlightDay:function(){

        this.$calendar.find('.day.selected').removeClass('selected');
        var day = this.model.get(this.options.daySelectAttribute);
        if(day == null){
            return false;
        }

        this.$calendar.find('.calendar-day-'+day.format('YYYY-MM-DD')).addClass('selected')

    },



    render:function(){

        this._super(false);

        this.$el.append(this.$toolbar);
        this.$el.append(this.$calendar);

        if(!this.options.static){
            this.trigger('render');
        }

        return this;
    },

    listenToDisplay:function(){

        this._super();

        this.$prevButton.on('click', _.bind(function(event){
            this.calendar.previous();
        },this));
        this.$nextButton.on('click', _.bind(function(event){
            this.calendar.next();
        },this));
        

    },

}, {


});


//ui.registerHelper('ui-input', _.bind(UICalendar.templateHelper, UICalendar)); 

module.exports = UICalendar;






