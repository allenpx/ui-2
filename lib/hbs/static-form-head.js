var handlebars = require('handlebars/runtime');module.exports = handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "    \"formTagId\":\""
    + container.escapeExpression(((helper = (helper = helpers.formTagId || (depth0 != null ? depth0.formTagId : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"formTagId","hash":{},"data":data}) : helper)))
    + "\",\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function";

  return "<script>window._ui_models = window._ui_models || [];\r\nwindow._ui_models.push({\r\n    \"id\":\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\",\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.formTagId : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    \"attributes\":"
    + ((stack1 = ((helper = (helper = helpers.attributes || (depth0 != null ? depth0.attributes : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"attributes","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ",\r\n    \"options\":"
    + ((stack1 = ((helper = (helper = helpers.options || (depth0 != null ? depth0.options : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"options","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ",\r\n    \"validation\":"
    + ((stack1 = ((helper = (helper = helpers.validation || (depth0 != null ? depth0.validation : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"validation","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n});\r\n</script>\r\n";
},"useData":true});