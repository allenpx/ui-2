var Base = require('./base');
var $ = require('jquery');
var _ = require('underscore');

var ui = require('./ui');

var Navigation = Base.extend({

    UIType:"navigation",

    initialize:function(options){

        this._super(_.extend({
            className:"Navigation",
            defaultValue:"",
            items:[]
        }, options));
          
        this.$items = $('<div/>');
        this.$items.attr("class", "ui-menu-items");

        this.itemsHash = {};

        this.on('render', function(){
            this.applyModelValueToDisplay();
        });

        if(this.options.closed){
            this.isOpen = false;
        }else{
            this.isOpen = true;
        }

        if(this.options.router){
            this.listenTo(this.options.router, "route", function(route, options){
                options = options || [''];
                if(options[0] == null){
                    options[0] = '';
                }
                var url = "#"+options.join('/').replace(/([^:]\/)\/+/g, "$1");
                //console.log(url, options)
                if(url.substr(-1) === '/') {
                    url =  url.substr(0, url.length - 1);
                }
                this.model.set('value', url);
            });
        }

    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    toggleOpen:function(){
        if(this.isOpen){
            this.close();
        }else{
            this.open();
        }
    },
    open:function(animated){
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.removeClass('ui-closed');
        this.$el.addClass('ui-open');
        this.isOpen = true;
        this.$el.height(this.$items.outerHeight());
        _.delay(_.bind(this.focus,this), 100);



    },

    close:function(animated){
        this.isOpen = false;
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.addClass('ui-closed');
        this.$el.removeClass('ui-open');
        this.$el.height('0px');
    },

    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-menu');
        this.$el.attr('tabindex', -1);
        this.$el.addClass(this.options.classes);
        this.$el.attr("data-ui", '');

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //render items
        this.renderItems();

        if(this.isOpen == false){
            this.close(false);
        }

        this.trigger('render');

        if(!this.options.static){
            this.listenToDisplay();
        }

        return this;
    },

    renderItems:function(){
        this.$items.empty();
        this.itemsHash = {};

        var buttonClasses = "ui-menu-item";
        if(this.options.classes.indexOf('no-style') != -1){
            buttonClasses += ' no-style';
        }

        _.each(this.options.items, function(itemObject, i){

            if(this.options.router && itemObject.href && typeof itemObject.value == 'undefined'){
                itemObject.value = itemObject.href;
            }
            
            var button = new ui.Button({
                classes:buttonClasses,
                label:itemObject.label,
                href:itemObject.href
            }).render();

            if(!this.options.static && this.options.route != 'undefined'){
                this.listenTo(button, 'action', function(){
                    this.model.set('value', itemObject.value);
                    this.trigger('select', {value:itemObject.value, button:button});
                }); 
            }

            button.on('mousedown', function(event){
                event.preventDefault();
                event.stopPropagation();
                return false;
            });

            this.$items.append(button.$el);
            this.itemsHash[itemObject.value.toString()] = button;

        },this);

        this.$el.append(this.$items);

    },


    setDisplayValue:function(value){
        value = value.toString();
        var match = false;
        var matchedItem = null;
        _.each(this.itemsHash, function(item, key){
            if(key == value){
                match = true;
                matchedItem = item;
                item.active(true);
            }else{
                item.active(false);
            }
        });

        if(!this.options.static){
            this.trigger('match', {
                isMatch:match,
                value:value,
                item:matchedItem
            });
        }
    },
    // getDisplayValue:function(){
    //     return this.$input.val();
    // },

    listenToDisplay:function(){
        this.$el.addClass("ui-init");
        ui.$body.on('focusin focus', '#'+this.id, _.bind(this.focusin, this));
        ui.$body.on('focusout', '#'+this.id, _.bind(this.focusout, this));
    },



    focus:function(event){
        $('#'+this.id).focus();
    },
    focusin:function(){
        this.$el.addClass('ui-focus');
    },
    focusout:function(event){
        this.$el.removeClass('ui-focus');
        if(this.options.closed){
            this.close();
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new Navigation(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        var elementFromHTML = new Navigation(options).render();
    }

});
  
ui.registerHelper('ui-select', _.bind(Navigation.templateHelper, Navigation)); 

module.exports = Navigation;






