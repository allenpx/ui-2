var Backbone = require('backbone');
var $ = require('jquery');
var _ = require('underscore');
var ui = require('./ui');
require('backbone-super');

var UILayout = Backbone.View.extend({

	namedElements:null,

 	initialize:function(options){

        this.options = _.extend({},{
            classes:'',
            model:null,
            attributes:null,
            validation:{},
            children:[],
            static:false
        }, options || {});

        this.options.self = this;

 		this.namedElements = {};

 	},

	build:function(){
		this.$el.empty();

		this.$el.append(this.buildFragment(this.options.children, this.$el));
	},

	buildFragment:function(childrenArray, $parentElement){
		UILayout.buildFragment(childrenArray, $parentElement, this.options);
	},

 	get:function(id){
 		return this.namedElements[id];
 	},

 	render:function(){

 		this.$el.addClass(this.options.classes);
 		this.build();

 		return this;
 	}

},{

    renderStatic:function(structure, options){

        options = options || {};
        structure = _.extend({},{
            classes:'',
            attributes:{},
            validation:{},
            children:[],
            formModelName:'form',
            static:true
        }, structure || {});

        structure.self = this;

        var $root = $('<div/>');
        var $el = $('<div/>');

        if(options.type == "form"){
            $el.addClass('ui-form');
        }
        // if(structure.formTagId){
        //     $el.attr('ui-form-tag-id', structure.formTagId);
        // }
        // if(structure.formModelName){
        //     $el.attr('ui-form-model', structure.formModelName);
        // }

        $el.append(UILayout.buildFragment(structure.children, $el, structure));
        $root.append($el);
        return $root.html();

    },

    buildFragment:function(childrenArray, $parentElement, options){

        if(Object.prototype.toString.call(childrenArray) === '[object Object]'){
            childrenArray = [childrenArray];
        }

        _.each(childrenArray, function(child){

            child.options = child.options || {};

            if(options.model && typeof child.options.model == 'undefined'){
                child.options.model = options.model;
            }
            
            if(typeof options.attributes != 'undefined'){
 
                if(typeof child.options.name != 'undefined' && typeof options.attributes[child.options.name] != 'undefined'){

                    child.options.value = options.attributes[child.options.name];

                    if(typeof options.validation != 'undefined' && typeof options.validation[child.options.name] != 'undefined'){
                        if(options.validation[child.options.name].required == true){
                            child.options.required = true;
                        }
                    }

                    if(typeof options.formErrors != 'undefined' && options.formErrors._isValid == false && typeof options.formErrors[child.options.name] != 'undefined'){
                        child.options.errorMessage = options.formErrors[child.options.name].message;
                    }
                    
                }

            }

            child.tagName = child.tagName || "div";
            child.type = child.type || '';
            child.options.static = child.options.static || options.static; 
            if(child.type.indexOf('ui.') != -1){
                var $item = new ui[child.type.split('.')[1]](child).render();
                //$item.$el.attr('id', child.name);
            }else if(child.ui){ 
                var $item = new ui[child.ui](child.options).render();
                //$item.$el.attr('id', child.name);
            }else if(child.view){ 
                var $item = new (child.view)(child.options).render();
            }else if(child.type == "html"){
                var $item = $('<div/>');

                if(child.html){
                    if(_.isArray(child.html)){
                        $item.html(child.html.join(''));
                    }else{
                        $item.html(child.html);
                    }
                }
                //$item.append(child.html); 
                //$item.html();
            }else{
                var $item = $('<'+child.tagName+'/>');
                $item.attr('id', child.name);
                $item.addClass(child.type);
            }
            
            if(!options.static && child.name){
                this.namedElements[child.name] = $item;
            }else{
                child.name = _.uniqueId("e");
            }
            

            if(!options.static && child.events){
                _.each(child.events, function(eventObject){
                    $('body').on(eventObject.event, '#'+child.name, eventObject.callback);
                });
            }

            if(child.classes){
                $item.addClass(child.classes);
            }

            if(child.text){
                $item.text(child.text);
            }

            if(child.iconLeft){
                $item.prepend('<i class="'+child.iconLeft+'"></i>&nbsp;');
            }

            if(child.children != undefined){ 
                if( _.keys(child.children).length ){
                    $item.append(UILayout.buildFragment(child.children, $item, options));
                }
            }

            if($item.$el != undefined){
                $parentElement.append($item.$el);
            }else{
                $parentElement.append($item);
            }

        }, options.self);

    },



});

module.exports = UILayout;
