var $ = require('jquery');
var _ = require('underscore');
var Handlebars = require('handlebars/runtime');
var Backbone = require('backbone');
Backbone.$ = $;
Backbone._ = _;
require('backbone-super');

// import element from 'virtual-element'
// import deku from 'deku'
// import jsxdom from 'jsxdom'
//var deku = require("deku").render;

if(!process.browser){
    //fix for jquery
    Backbone.View.prototype._createElement = function(tagName){
        return $('<'+tagName+'/>')[0]; 
    }
}else{
    window.$ = $;
    window.Handlebars = Handlebars;
    window.jQuery = $;
}

var events = require('inherit/events');


_.mixin({
	toBoolean:function(value){
		if(value.toLowerCase() === "true"){
			return true;
		}else{
			return false;
		}
	}
});

if(!String.prototype.trim) {
    (function() {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function() {
          return this.replace(rtrim, '');
        };
    })();
}     



var UI = events.extend({

    // jsx:{
    //     render:function(element, dom){ 
    //         deku.render(deku.tree(element), dom)
    //     },
    //     dom:jsxdom
    // },

	initialize:function(options){

		options = options || {};
		this.options = _.extend({
			exportGlobal:false
		}, options);

        this.static = false;
        this.$body = $('body');
        this.$floatingLayer = $('<div class="ui-floating-layer"></div>');
        this.$body.append(this.$floatingLayer);
		this.on('destroy', this.destroy, this);

	}, 

	render:function($context){

		var self = this;

		$context = $context || $('body');

        //ui.registerForms($context);

        //find all static elements and initialize them
		$context.find('*[data-ui]:not(.ui-init):not(.ui-child)').each(function(index, element){
			ui.renderFromStaticHTML(element);
		});

		// _.each(this.selectors, function(selectorFunc, selectorStr){
		// 	$context.find(selectorStr).each(function(i,element){
		// 		selectorFunc($(element));
		// 	});
		// },this);

        

	},


	elements:{},

	get:function(id){
		return this.elements[id];
	},

	registerComponent:function(component){
		this.elements[component.id] = component;
	},

	destroy:function(element){
		delete this.elements[element.id];
	},

    destroyInContext:function($context){
        $context.find('.ui-init').each(function(index, element){
            var uiEl = ui.get(element.id);
            if(typeof uiEl != 'undefined' && uiEl != null){
                uiEl.destroy();
            }
        });
    },

	selectors:{},
	registerSelector:function(selector, callback){
		this.selectors[selector] = callback;
	},

	helpers:{},
	registerHelper:function(selector, helperFunc){
		this.helpers[selector] = helperFunc;
	},
	registerHelpers:function(handlebarsInstance){
		if(typeof handlebarsInstance == "undefined"){
			handlebarsInstance = Handlebars;
		}
		_.each(this.helpers, function(helperFunc, selector){
			handlebarsInstance.registerHelper(selector, helperFunc);
		});
	},


    models:{},
    registerModel:function(modelObj){
        //console.log(modelObj) 
        var modelOptions = modelObj.options || {};
        modelOptions.formTagId = modelObj.formTagId;
        modelOptions.validation = modelObj.validation;
        console.log(modelOptions)
        this.models[modelObj.id] = new ui.Model(modelObj.attributes, modelOptions);
    },
    registerModels:function(){
        _.each(window._ui_models, function(modelObj){
            this.registerModel(modelObj);
        },this);
    },
    getModel:function(modelName){
        return this.models[modelName];
    },

    forms:{},
    formElements:{},
    registerForms:function($domScope){
        
        $domScope = $domScope || $('body');
        var self = this;
        $domScope.find('form:not(.ui-init)').each(function(index, element){

            var $form = $(element);
            $form.addClass('ui-init');
            var id = element.id.toString() || _.uniqueId('form');
            var formModel = self.models[$form.attr('data-model')]
            self.forms[id] = $form;
            self.formElements[id] = {};

            $form.on('submit', function(e){
                _.each(self.formElements[id], function(el){
                    el.dirty();
                });
                var isValid = formModel.isValid();

                if(isValid == false){
                    e.preventDefault();
                    var delayCount = 0;
                    _.each(self.formElements[id], function(el){
                        if(el.isError){
                            delayCount += 1;
                            _.delay(function(){
                                el.flashError();
                            }, 100*delayCount);
                        }
                    });
                }else{
                    return true;
                }

            })

        });

    },



	html:function($element){
		if(process.browser){
			return $element[0].outerHTML;
		}else{
			return $.html($element);
		}
	}, 
	htmlSafe:function($element){
		return new Handlebars.SafeString(ui.html($element));
	},


    getStaticBootCheck:function(){
        return 'window.__uis__=typeof window.__uis__!="undefined"?window.__uis__:{};';
    },

    getStaticTag:function(id, options){ 
        return ui.getStaticBootCheck()+'window.__uis__["'+id+'"] ='+JSON.stringify(options)+';';
    },

    getStaticOptions:function(id){ 
        if(typeof window.__uis__ == 'undefined' || typeof window.__uis__[id] == 'undefined'){
            return null;
        }else{
            return window.__uis__[id];
        }
    },

    renderFromStaticHTML:function(el){
        console.log(el)
        var options = ui.getStaticOptions(el.getAttribute('id'));
        if(typeof options == 'undefined' || options == null){
            return null;
        }
        options.static = false;
        options.el = el;
        var Element = new ui[el.getAttribute('data-ui')](options).render();
        window.test = Element;
    }


    // slideOpen:function($parent, $child){
    //     $slideOpen.height($parent.height());
    //     $parent.addClass('anim');

    //     //set parent to child height
        

    // },
    // slideClose:function($parent, $child){

    // }


});

var ui = new UI();

module.exports = ui;


ui.Base = require('./base');
ui.Button = require('./button');
ui.Dialog = require('./dialog');
ui.Layout = require('./layout');
ui.Model = require('./model');
ui.Calendar = require('./calendar');
ui.Toggle = require('./toggle');
ui.RadioGroup = require('./radio-group');
ui.Menu = require('./menu');
ui.Input = require('./input');
    ui.Textfield = ui.Input;
ui.TextArea = require('./textarea');
ui.Select = require('./select');
ui.StaticForm = require('./static-form');

//debug elements
ui.FocusTest = require('./focus-test');
ui.FocusTestChild = require('./focus-test-child');

if(process.browser){
    ui.registerModels();
	ui.registerHelpers();
}

