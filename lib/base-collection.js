var Backbone = require('backbone');
var $ = require('jquery');
var _ = require('underscore');

var ui = require('./ui');
var Model = require('./model');
var Base = require('./base');

var BaseCollection = Base.extend({

    initialize:function(options){
 
        this._super(options);

    },

    initInternalModel:function(){

        this._super();

        this.options.collection = this.options.collection || [];

        if(_.isArray(this.options.collection)){
            this.collection = new Backbone.Collection(this.options.collection);
        }else{
            this.collection = this.options.collection;
        }

        this.listenTo(this.collection, 'change', this.render);

    },


});

module.exports = BaseCollection;