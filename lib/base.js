/** @jsx ui.jsx.dom */
var Backbone = require('backbone');
var $ = require('jquery');
var _ = require('underscore');
var ui = require('./ui');
var Model = require('./model');

var template = require('./hbs/wrapper');

var Base = Backbone.View.extend({

    UIElement:true,
    UIType:"base",
    suppressFocusout:false,

    initialize:function(options){
        this._super();
        this.options = _.extend({
            _options:options,
            className:"base",
            'classes':'',
            'static': ui.static,
            focusable:true,
            modelOptions:{},
            externalElements:[],
            floatParent:null,
            floatPosition:'bottom'
        }, options);
        
        //set the id
        if(this.options.el && this.options.el[0] && this.options.el[0].id){
            this.id = this.options.el[0].id;
        }else if(typeof this.options.id != 'undefined'){
            this.id = this.options.id;
        }else{ 
            this.id = _.uniqueId('ui'); 
        }

        $(this.el).attr('id', this.id);
        $(this.el).attr('data-ui', this.options.className);
        this.name = this.options.name || '_'+this.id; // create name from id

        if(_.isString(this.options.model)){
            $(this.el).attr('data-ui-model', this.options.model);
        }
        if(this.options.attribute){
            $(this.el).attr('data-ui-attribute', this.options.attribute);
        }

        //set up models
        if(this.options.static != true){
            if(_.isString(this.options.model)){
                this.options.model = ui.getModel(this.options.model);
            }
            if(this.options.initInternalModel != false){
                this.initInternalModel();
                this.bindExternalModel();
            }
            ui.registerComponent(this);
        }

        if(!this.options.static){
            this.on('render', _.bind(this.onRender, this));
            if(this.options.floatParent){
                $(window).on('scroll', _.bind(this.positionToFloatParent, this));
                this.positionToFloatParent();
            }
        }

    },

    positionToFloatParent:function(){ 
        if(this.options.floatPosition == 'bottom'){
            //this.$el.css('top', this.options.floatParent.$el.offset().top+this.options.floatParent.$el.height()-parseInt(this.options.floatParent.$el.css('margin-bottom')));
            this.$el.css('top', this.options.floatParent.$el.offset().top+this.options.floatParent.$el.height()-parseInt(this.options.floatParent.$el.css('margin-bottom')));
            this.$el.css('left', this.options.floatParent.$el.offset().left);
        }
    },

    render:function(trigger){

        this.$el.empty();
        this.$el.addClass(this.options.baseClass);
        this.$el.addClass(this.options.classes);
        this.$el.attr('id', this.id);
        if(this.options.static){
            this.$el.append(ui.getStaticTag(this.id, this.options._options));
        }

        if(this.options.focusable){
            this.$el.attr('tabindex', '-1');
        } 

        if(this.staticModel){
            this.$el.attr('data-ui-model', this.staticModel);
        }

        if(!this.options.static){
            if(this.isBound == false){
                this.listenToDisplay();
            }
        }

        if(trigger != false){
            this.trigger('render');
        }

        return this;

    },

    onRender:function(){
        this.applyModelValueToDisplay();
        this.validateModel();
    },

    beforeInternalModelInit:function(){}, // overide

    initInternalModel:function(){

        this.beforeInternalModelInit();
        this.attribute = 'value';
        this.model = new Model({
            value:this.options.value || this.options.defaultValue || ''
        }, this.options.modelOptions || {});
        this.listenTo(this.model, 'change:value', this.applyModelValueToDisplay);
        this.listenTo(this.model, 'validate', this.validated);

    },

    bindExternalModel:function(){
        if(typeof this.options.attribute == 'undefined'){
            this.options.attribute = this.options.name;
        }
        if(this.options.model && this.options.attribute){
            this.attribute = this.options.attribute;
            if(typeof this.options.modelOptions == 'undefined' || typeof this.options.validation == 'undefined' || typeof this.options.modelOptions.validation == 'undefined'){
                this.stopListening(this.model, 'validate:value');
            }
            this.listenTo(this.options.model, 'change:'+this.options.attribute, this.applyExternalModelValueToModel);
            this.listenTo(this.options.model, 'validate', this.validated);
            this.applyExternalModelValueToModel();
        }else{
            this.model.set('value', this.options.value || this.options.defaultValue || "");
        }
    },

    validateModel:function(){
        if(this.options.model && this.options.attribute){
            this.options.model.validate();
        }else{
            this.model.validate();
        }
    },

    applyModelValueToExternalModel:function(event){
        this.options.model.set(this.options.attribute, this.model.get('value'));
    },
    applyExternalModelValueToModel:function(event){
        this.model.set('value', this.options.model.get(this.options.attribute));
    },
    applyDisplayValueToModel:function(event){
        this.model.set('value', this.getDisplayValue());
    },
    applyModelValueToDisplay:function(){
        var val = this.model.get('value');
        this.setDisplayValue(val);

        if(this.options.model && this.options.attribute){
            this.applyModelValueToExternalModel();
        }

    },

    isError:false,
    error:function(arg){
        if(_.isBoolean(arg)){
            if(arg){
                this.displayError(true);
            }else{
                this.displayError(false);
            }
        }else if(_.isString(arg)){
            this.displayError(true, arg);
        }else{
            this.displayError(false);
        }

    },

    isClean:true,
    dirty:function(){
        this.isClean = false;
    },

    flashError:function(){
        if(this.isError){
            this.$el.addClass('ui-error-show');
            // var self = this;
            // _.delay(function(){
            //     self.$el.removeClass('ui-error-show');
            // },500);
        }
    },




    /*
    
  ,ad8888ba,                                                88          88
 d8"'    `"8b                                               ""          88
d8'        `8b                                                          88
88          88 8b       d8  ,adPPYba, 8b,dPPYba, 8b,dPPYba, 88  ,adPPYb,88  ,adPPYba,
88          88 `8b     d8' a8P_____88 88P'   "Y8 88P'   "Y8 88 a8"    `Y88 a8P_____88
Y8,        ,8P  `8b   d8'  8PP""""""" 88         88         88 8b       88 8PP"""""""
 Y8a.    .a8P    `8b,d8'   "8b,   ,aa 88         88         88 "8a,   ,d88 "8b,   ,aa
  `"Y8888Y"'       "8"      `"Ybbd8"' 88         88         88  `"8bbdP"Y8  `"Ybbd8"'


     */
    getDisplayValue:function(){
        return true;
    },
    setDisplayValue:function(value){

    },

    focusoutTimeout:null,
    isBound:false,
    listenToDisplay:function(){ 
        
        if(this.isBound == true){
            return false;
        }
        this.isBound = true;
        this.$el.addClass("ui-init");
        
        $('body').on('focusin.'+this.id, '#'+this.id, _.bind(function(event){
            //console.log(this.options.className+':focusin'); 
            if(this.isFocused == false){
                this.focusin();
                //this.trigger('_focusin');
            }
        }, this));

        $('body').on('focusout.'+this.id, '#'+this.id, _.bind(function(event){
            //console.log(this.options.className+':focusout')
            clearTimeout(this.focusoutTimeout);
            this.focusoutTimeout = setTimeout(_.bind(function() {
                var childHasFocus = this.hasFocus();
                var externalChildFocus = false;
                if(this.options.externalElements){
                    _.each(this.options.externalElements, function(externalElement){
                        if(externalElement.hasFocus()){
                            externalChildFocus = true;
                        }
                    },this);
                }
                if (childHasFocus == false && externalChildFocus == false) {
                    if(!this.suppressFocusout){
                        //console.log('_focusout')
                        this.focusout();
                    }
                }else{

                }
            },this), 10); 

        }, this));

        //this.on('_focusin', this.focusin);
        // this.on('_focusout', function(){
        //     this.focusout();
        // });
        

    },

    hasFocus:function(){
        return (this.$el.find(':focus').length > 0 || this.$el.is(':focus') );
    },

    validated:function(event){ 
        if(typeof event != 'undefined' && event.isValid == false){
            if(event.displayErrors == true){
                this.dirty();
            }
            if(event.errors[this.attribute] && event.errors[this.attribute].valid == false){
                this.error(event.errors[this.attribute].message);
            }
        }else{
            this.error(false);
        }
    },

    displayError:function(isError, message){

    },

    focus:function(){
        $('#'+this.id).focus();
    },
        
    isFocused:false,
    focusin:function(trigger){
        this.isFocused = true;
        this.$el.addClass('ui-focus');
        this.focus();
        if(trigger != false){
            this.trigger('focusin');
        } 
    },

    focusout:function(trigger){
        this.isFocused = false;
        this.$el.removeClass('ui-focus');
        this.dirty();
        if(trigger != false){
            this.trigger('focusout');
        }

        if(this.options.model){
            this.options.model.validate();
        }else{
            this.model.validate();
        }
    },

    /*
    when firefox/ie catches up, this might work,
    but due to their lack of relatedTarget, the above hack must be used
    */
    /*
    focusout:function(event, trigger){
        console.log(event)
        if(
            this.suppressFocusout ||
                (
                    (event.relatedTarget === this.$el[0] && $.contains(this.$el[0], event.target)) ||
                    (event.target === this.$el[0] && $.contains(this.$el[0], event.relatedTarget))
                ) ||
                (
                    $.contains(this.$el[0], event.relatedTarget) && 
                    $.contains(this.$el[0], event.target)
                )

        ){

            return false;
        }else{
            this.isFocused = false;
            this.$el.removeClass('ui-focus');
            if(trigger != false){
                this.trigger('focusout');
            }
            return true;
        }

    },
    */

    destroy:function(){

        $("body").off( "."+this.id );
        this.undelegateEvents();
        this.stopListening();
        this.$el.removeData().unbind(); 
        this.remove();
        Backbone.View.prototype.remove.call(this);

        if(this.options.externalElements.length){
            _.each(this.options.externalElements, function(externalElement){
                if(externalElement.destroy){
                    externalElement.$el.remove();
                    externalElement.destroy();
                }
            },this);
        }
        ui.destroy(this);
        
    },
    getConstructor:function(){
        return Base;
    },

    getStaticHelper:function(){
        // var r = null;
        // if(this.options.static){
        //     r = <script>{ui.getStaticTag(this.id, this.options._options)}</script>;
        // }
        // return r;
    },

    wrap:function(childHTML, options){ 
        childHTML = childHTML || null;
        options = options || {};
        var childOptions = {};
        if(_.isFunction(childHTML)){
            childOptions.childHTML = childHTML(options);
        }else if(typeof childHTML != 'undefined'){
            childOptions.childHTML = childHTML;
        } 
        console.log(_.extend({}, options, childOptions))
        return template(_.extend({}, options, childOptions));
    }

}, {



});

module.exports = Base;